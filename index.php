<?php include('include/global/head.php'); ?>

  <div class="wrap intro">
    <div class="video video-resume" style="display: none;">
      <video id="video_resume" autoplay="true" loop="true" muted="true">
        <source src="/assets/videos/static.mp4" type="video/mp4"></source>
      </video>
    </div>
    <div class="intro-image scene_element scene_element--fadein"></div>
    <h1 class="scene_element scene_element--fadeinup scene_element--delay">Dy<span class="lava lava-launch">la</span>n <span class="flicker">Na<span class="lava">va</span>jas</span> <span id="lock_hide">G</span><span id="lock_hover">luck</span></h1>
    <div class="intro-paragraph scene_element scene_element--fadeinup scene_element--delay">
      <p>Developer &amp; Product Leader at <a href="http://readysetrocket.com/" id="video_toggle_rsr" target="_blank">Ready Set Rocket</a> in New York, NY.</p>
      <p>Check out my <a href="/downloads/dylan_gluck_resume_121216.pdf" id="video_toggle_resume" target="_blank">Resume</a>, or just look around.</p>
    </div>
    <p class="hint"><em>*press enter for command line*</em></p>
    <div class="block-overlay scene_element scene_element--fadeinleft scene_element--delay"><div class="inner"><div class="height"></div></div></div>
  </div>

  <div class="lock-wrap">
    <i class="fa fa-lock"></i>
    <span class="question" style="display:none;">Are you afraid of the dark? <a href="javascript:void(0)" id="dark_yes">yes</a> <a href="javascript:void(0)" id="dark_no">no</a></span>
  </div>


<?php include('include/global/foot.php'); ?>

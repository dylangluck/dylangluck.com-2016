(function(module){

  'use strict';

  var CSS_DIR   = 'assets/styles/';
  var JS_DIR    = 'assets/scripts/';

  module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      // We provide globals to meta so they can be used dynamically in strings for property names.
      meta: {
        css: CSS_DIR,
        js: JS_DIR
      },
      less: {
        development: {
          files: {
            "<%= meta.css %>custom.min.css": "<%= meta.css %>custom/custom.less"
          }
        }
      },
      cssmin: {
        compress: {
          options: {
            banner: '/* Minified and Combined CSS */'
          },
          files: {
            '<%= meta.css %>libs.min.css': []
          }
        }
      },
      uglify: {
        options: {
          mangle: false,
          compress: false
        },
        jsmin: {
          files: {
            '<%= meta.js %>libs.min.js': [
              '<%= meta.js %>libs/jquery-1.11.2.min.js',
              '<%= meta.js %>libs/modernizr.custom.js'
            ],
            '<%= meta.js %>custom.min.js': [
              '<%= meta.js %>custom/custom.js',
              '<%= meta.js %>custom/_section-home.js'
            ]
          }
        }
      },
      watch: {
        css: {
          files: '<%= meta.css %>/custom/*.less',
          tasks: ['less:development']
        },
        js: {
          files: '<%= meta.js %>/custom/*.js',
          tasks: ['uglify:jsmin']
        }
      }
    });

    // ////////////////// //
    // LOAD GRUNT MODULES //
    // ////////////////// //

    // Less to CSS
    grunt.loadNpmTasks('grunt-contrib-less');

    // Minify CSS
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Uglify for minification
    grunt.loadNpmTasks('grunt-contrib-uglify');


    // //////////////////// //
    // REGISTER GRUNT TASKS //
    // //////////////////// //

    // Default task(s).
    grunt.registerTask('default', ['build']);
    grunt.registerTask('build', ['less:development', 'cssmin:compress', 'uglify:jsmin']);

    // Minify CSS files only
    grunt.registerTask('css', ['less:development', 'cssmin:compress']);

    // Minify JS files only
    grunt.registerTask('js', ['uglify:jsmin']);

  };

}(module));

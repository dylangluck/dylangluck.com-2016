//
// Homepage Logic
//

// Desktop Only
if(!isMobile) {
  // Resume Hover
  $('#video_toggle_resume').mouseenter(function(){
    $('.video-resume').show().find('video').removeAttr('muted');
    $('.intro-image').addClass('white');
    $('.hint').show();
    $('.intro-paragraph').css('color', 'transparent').find('a').css('border-color', 'transparent');
  }).mouseleave(function(){
    $('.video-resume').hide().find('video').attr('muted', 'true');
    $('.intro-image').removeClass('white');
    $('.hint').hide();
    $('.intro-paragraph').css('color', '#000').find('a').css('border-color', '#000');
  });
  // RSR Hover
  $('#video_toggle_rsr').mouseenter(function(){
    $('.intro-image').css('background-image', 'url(http://static1.businessinsider.com/image/5542343cecad04291bbdc387/rocket-lift-off-gif.gif)');
  }).mouseleave(function(){
    $('.intro-image').css('background-image', 'url(/assets/images/popular-landscaping-desert-landscape-coloring-pages-desert-landscape-materials-desert-landscape-mounds-desert-landscape-mulch-desert-landscape-murals-desert-landscape-magazine-desert-landscape-m.jpg)');
  });
  // Black Box Hover
  var navajasFlicker;
  function flicker(){
    if($('h1 .flicker').css('opacity') == '1'){
      $('h1 .flicker').css('opacity', '0');
    } else {
      $('h1 .flicker').css('opacity', '1');
    }
  }
  $('.block-overlay').mouseenter(function(){
    $('.intro-image').hide();
    navajasFlicker = window.setInterval(flicker, 50);
    $('.intro-paragraph').css('top', '50%');
  }).mouseleave(function(){
    $('.intro-image').show();
    clearInterval(navajasFlicker);
    $('h1 .flicker').css('opacity', '1');
    $('.intro-paragraph').css('top', '10%');
  });
  // LA VA Hover
  $('.lava-launch').mouseenter(function(){
    $('h1').css('color', 'transparent').find('.lava').css('color', '#000');
    $('.intro-image').css('background-image', 'url(https://media.giphy.com/media/lz9Ems3xHGOSA/giphy.gif)');
  }).mouseleave(function(){
    $('.intro-image').css('background-image', 'url(/assets/images/popular-landscaping-desert-landscape-coloring-pages-desert-landscape-materials-desert-landscape-mounds-desert-landscape-mulch-desert-landscape-murals-desert-landscape-magazine-desert-landscape-m.jpg)');
    $('h1').css('color', '#000');
  });
  // Lock
  setTimeout(function(){
    $('#lock_hover').css('cursor', 'pointer').mouseenter(function(){
      $(this).text('lock');
      $('#lock_hide').css('opacity', '0');
      $('.lock-wrap').addClass('show').click(function(){
        $(this).find('i').removeClass('fa-lock').addClass('fa-unlock');
        $(this).find('.question').fadeIn();
        $('#dark_yes').click(function(){
          $('body').removeClass('dark');
        });
        $('#dark_no').click(function(){
          $('body').addClass('dark');
          $('.intro-image').css('background-image', 'url(http://emj.bmj.com/content/23/5/e32/F2.large.jpg)');
        });
      });
    }).mouseleave(function(){
      $(this).text('luck').off('mouseenter mouseleave').css('cursor', 'default');
      $('#lock_hide').css('opacity', '1');
    });
  }, 1000);
  // Command Prompt
  $(document).keyup(function(evt){
    if(evt.keyCode == 13 && $('h1 input').length < 1) {
      $('h1').html('<input type="text"></input>').find('input').focus();
    } else if(evt.keyCode == 13) {
      var command = $('h1 input').val();
      command = encodeHTML(command).toLowerCase();
      switch (command) {
        case 'sepia':
            $('.intro-image').css('background-image', 'url(https://upload.wikimedia.org/wikipedia/commons/6/65/Sepia-landscape.JPG)');
            $('body').css('color', '#543B3B');
            $('.block-overlay .height').css('background-color', '#543B3B');
          break;
        case 'fast':
            $('.intro-image').css('background-image', 'url(http://theamericangenius.com/wp-content/uploads/2014/06/eddie.gif)');
          break;
        case 'new-york':
            $('.intro-image').css('background-image', 'url(https://media.giphy.com/media/3oEduRrxk255EewViw/giphy.gif)');
          break;
        case 'portfolio':
            showPortfolio();
          break;
        case 'help':
            showHelp();
          break;
        default:
            showHelp();
          break;
      }
      $('h1').html('Dylan Navajas Gluck');
    }
  });
  // Show Portfolio
  function showPortfolio() {
    var links = [
      'http://youramerica.com/',
      'https://axeljohnson.com/',
      'http://perfumaniaholdings.com/',
      'http://www.kearnypoint.com/flex-spaces/',
      'http://upfront2016.corporate.univision.com/',
      'http://readysetrocket.com/'
    ];
    $('.portfolio-links, .help-commands').remove();
    $('.intro-paragraph').append('<ul class="portfolio-links"></ul>');
    $.each(links, function(i, link) {
      var $el = '<li><a href="' + link + '" target="_blank">' + link + '</a></li>';
      $('.portfolio-links').append($el);
    });
  }
  // Show Help
  function showHelp() {
    var commands = [
      'help',
      'potfolio',
      'fast'
    ];
    $('.portfolio-links, .help-commands').remove();
    $('.intro-paragraph').append('<ul class="help-commands"><li>commands:</li></ul>');
    $.each(commands, function(i, command) {
      var $el = '<li>' + command + '</a></li>';
      $('.help-commands').append($el);
    });
  }
}
